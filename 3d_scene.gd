extends Spatial


	# I have a PackedScene with a number of Control nodes, including a ColorRect
	# and a RichTextLabel. I wish to load up this scene, configure the nodes
	# according to various paramters, such as changing the text on the label.
	# I then want to capture an image of that scene for use elsewhere.

	# Ultimately, the goal is to generate a texture on the fly, with text on it
	# that describes the state of the game world in some way, and use this as a
	# texture on a MeshInstance.


var scene_control = preload("res://control_root.tscn")
var scene_viewport = preload("res://viewport_root.tscn")


func _ready():
	# Method 1.

	# Instantiate the 2D scene.
	var control_root = scene_control.instance()

	# Make changes to the scene.
	control_root.get_node("RichTextLabel").text = "Toast."

	# Capture an image of the 2D scene (and save it to a PNG for debugging).
	# This produces an error, as the Control has no viewport.
	#control_root.get_viewport().get_texture().get_data().save_png("res://" + str(OS.get_unix_time()) + "_toast.png")


	# Method 2.

	# Instantiate the 2D scene.
	var viewport_root = scene_viewport.instance()

	# Make changes to the scene.
	viewport_root.get_node("Control/RichTextLabel").text = "Crunch."
	
	# Capture an image of the 2D scene (and save it to a PNG for debugging).
	# This outputs a completely black image.
	viewport_root.get_texture().get_data().save_png("res://" + str(OS.get_unix_time()) + "_crunch.png")


	# Method 3.

	# Instantiate the 2D scene.
	var viewport_root_child = scene_viewport.instance()

	# Make changes to the scene.
	viewport_root_child.get_node("Control/RichTextLabel").text = "Snake."

	# Add the instance to the tree.
	add_child(viewport_root_child)

	# Capture an image of the 2D scene (and save it to a PNG for debugging).
	# This outputs a completely black image.
	viewport_root_child.get_texture().get_data().save_png("res://" + str(OS.get_unix_time()) + "_snake.png")


	# Method 4.

	# Instantiate the 2D scene.
	var viewport_root_idle = scene_viewport.instance()

	# Make changes to the scene.
	viewport_root_idle.get_node("Control/RichTextLabel").text = "Badger."

	# Add the instance to the tree. This seems to be necessary for it to draw.
	add_child(viewport_root_idle)

	# Wait until the draw phase of the frame is done, I guess?
	yield(VisualServer, "frame_post_draw")

	# Capture an image of the 2D scene (and save it to a PNG for debugging).
	# This outputs the expected image.
	viewport_root_idle.get_texture().get_data().save_png("res://" + str(OS.get_unix_time()) + "_badger.png")
